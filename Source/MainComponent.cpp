/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    //audio
    audioDeviceManager.initialiseWithDefaultDevices(2, 2); //2in 2out
    audioDeviceManager.addAudioCallback(this);
    //MIDI
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    //visual
    gain.setRange(0.0, 1.0);
    gain.setSliderStyle(juce::Slider::LinearVertical);
    addAndMakeVisible(gain);
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    gain.setBounds(0, 0, getWidth()/5.0, getHeight());
}
void MainComponent::audioDeviceIOCallback (const float** inputChannelData,
                            int numInputChannels,
                            float** outputChannelData,
                            int numOutputChannels,
                            int numSamples)
{

    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float fGain = gain.getValue();
    fGain = fGain * fGain;
    
    while (numSamples--)
    {
        *outL = *inL * mod;
        *outR = *inR * mod;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}
void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("audio device about to start");
}
void MainComponent::audioDeviceStopped()
{
    DBG("audio device stopped");
}
void MainComponent::handleIncomingMidiMessage(MidiInput* source,
                                              const MidiMessage& message)
{
    DBG("incoming MIDI message");
    if (message.getControllerNumber() == 1) {
        mod = message.getControllerValue()/127.0;
        DBG("mod = " << mod);
        gain.getValueObject().setValue(mod);
    }
}