/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class MainComponent   : public Component,
                        public AudioIODeviceCallback,
                        public MidiInputCallback

{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();
    
    void resized();
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples);
    
    void audioDeviceAboutToStart (AudioIODevice* device);
    
    void audioDeviceStopped();
    
    void handleIncomingMidiMessage (MidiInput* source,
                                    const MidiMessage& message) override;
    
    
private:

    AudioDeviceManager audioDeviceManager;
    Slider gain;
    float mod = 0;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
